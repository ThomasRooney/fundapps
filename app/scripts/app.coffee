'use strict'

###*
 # @ngdoc overview
 # @name fundappsApp
 # @description
 # # The entry point to the application
 #
 # Main module of the application.
###

app = angular.module 'fundappsApp', ['ngRoute', 'ui.grid']

days_look_back = 30

# Service module to access data
app.factory 'Data', ($http) -> 
  factory = 
    _tryGet: (urls, cb) ->
      if urls.length <= 0
        cb false
        return
      url = urls[0]
      @_get(url)
        .success((response) -> cb true, response, days_look_back-urls.length)
        .error(=> @_tryGet(urls.slice(1), cb))
    _get: (dataUrl) ->
      $http({
        url: dataUrl
        method: "GET"
        transformResponse: (result) ->
          return result # json returned isn't actually json, it's a javascript object. eval it.
          # This is also very unsafe, especially if you're not in https
      })
    getLists: -> 
      @_get("data/new-lists.json").then (response) -> eval(response.data)
    getListDetail: (list, date, cb) ->
      date = new Date()

      dates = []
      for i in [0..days_look_back]
        date.setDate(date.getDate() - 1)
        dates.push("data/new-lists/#{list}/#{date.toJSON().slice(0,10)}.json")

      @_tryGet dates, (success, response, days_since) ->
        if success
          cb success, eval(response), days_since
        else 
          cb false
  return factory


app.controller 'ListDetailCtrl', ($scope, Data, $routeParams) ->
  console.log $scope, $routeParams
  $scope.heading = "Loading #{$routeParams.who}"
  $scope.columns = [
        {
          name: "fields.isin"
          displayName: "ISIN"
        },
        {
          name: "fields.name"
          displayName: "Name"
        },
        {
          name: "fields.role"
          displayName: "Role"
        },
        {
          name: "wentOn"
          displayName: "Violation Start"
        },
        {
          name: "wentOff"
          displayName: "Violation End"
        },
      ]
  Data.getListDetail $routeParams.who, $routeParams.date, (success, data, daysSince) ->
    console.log data
    if success
      if daysSince > 0
        $scope.heading = "#{$routeParams.who} -- Data is Stale (from #{daysSince} days ago)"
      else 
        $scope.heading = routeParams.who
      $scope.listData = data
    else 
      $scope.heading = "Failure -- no additional data found"

app.controller 'ListCtrl', ($scope, Data, $location) ->
  Data.getLists().then (result) -> 
    console.log result
    $scope.clickHandler = {
      onClick: (value) -> console.log value; $location.path( value );
    } 
    $scope.rawData = result
    $scope.listData = filter(result)
    $scope.columns = [
        {
          name: "displayName"
          displayName: "Name"
        },
        {
          name: "category"
          displayName: "Category"
        },
        {
          name: 'href'
          displayName: 'Additional Detail'
          cellTemplate: '<button ng-click="grid.appScope.clickHandler.onClick(row.entity.href)" class="fullScale">
                         Additional Detail
                         </button>'
        }
      ]
    $scope.grid = 
      data: $scope.listData
      columnDefs: $scope.columns

# Router
app.config ($routeProvider) ->
  $routeProvider.
    when('/initial', {
      templateUrl: 'views/initial.html'
      controller: 'ListCtrl'
      }).
    when('/new-lists/:who/:date.json',
      templateUrl: 'views/new_lists.html'
      controller: 'ListDetailCtrl').
    otherwise(
      redirectTo: '/initial'
    )

fixLink = (href) -> 
  toReplace = "/{effectiveOn}"
  fixed = href.replace(toReplace, "") + "/" + new Date().toJSON().slice(0,10) + ".json"
  return fixed

filter = (list_data) -> 
  for e in list_data
    e.href = fixLink(e.href)
  return list_data
