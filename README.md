# fundapps

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Prerequisites

    npm install -g grunt-cli bower

### For Development

    npm install -g  yo generator-karma generator-angular

## Build & development

To do first-time setup, run `npm install` and `bower install` from the root directory.

Run `grunt` for building and `grunt serve` for preview.
